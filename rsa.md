TITLE XXI\
MOTOR VEHICLES
=========================

CHAPTER 266\
EQUIPMENT OF VEHICLES
----------------------------------

Miscellaneous
-------------

### Section 266:58-a

    **266:58-a Tinted Glass. --**\
I. It shall be unlawful to sell or inspect any motor vehicle in this state which has after market tinting on the windshield or on the windows to the left and right of the driver.\
II. It shall be unlawful to install after market tinting on the windshield or on the windows to the left and right of the driver on any motor vehicle which is registered in this state.\
III. It shall be unlawful to drive on any way any motor vehicle registered in this state which has after market tinting on the windshield or on the windows to the left and right of the driver. Where after market tinting is applied to windows to the rear of the driver, outside rear view mirrors shall be required on both the left and right side of the vehicle for the use of the driver and a front seat passenger. The light transmittance of after-market tinted windows where they are allowed shall not be less than 35 percent, except that the light transmittance of aftermarket tinted rear windows of multipurpose passenger vehicles, as defined in 49 C.F.R. 571.3 and pickup trucks may be such percentage as is allowed by 49 C.F.R. 571.205 with respect to pre-market tinted rear windows.\
III-a. Persons who require for medical reasons after market tinting on the windshield or on the windows to the left and right of the driver may apply for a special permit pursuant to RSA 266:61-a, IX.\
IV. The commissioner shall adopt rules under RSA 541-A relative to the administration and enforcement of this section.\
V. Nothing in this section shall be construed to prohibit after market tinting of the windshield of a vehicle with a strip not wider than 6 inches located at the very top of the windshield, provided that the light transmittance of the strip shall not be less than 35 percent.\
VI. Any natural person or any other person who violates the provisions of this section shall be guilty of a violation.

**Source.** 1989, 160:1. 1990, 44:1. 1997, 252:4, eff. Aug. 18, 1997.
